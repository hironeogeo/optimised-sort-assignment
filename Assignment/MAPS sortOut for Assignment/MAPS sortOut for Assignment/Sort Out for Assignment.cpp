/***********************************************************************
MAPS Assignment 1 - Starting Point for OMP/SIMD Assignment

SortOut - integer sorting and file output program.

The data is held in 2000 rows of 1000 numbers each. Each row is sorted independently,
currently using a simple bubble sort.

Your version should also include sorting all of the data at once, i.e. all 2,000,000 numbers,
and this can be done after the rows have been sorted.

Outputting the strings should employ SIMD to exploit hardware parallelism.

This version includes basic timing information and uses strings to create the file output.

S.Andrews / A.Oram
Revised: A.Oram Feb 2018
************************************************************************
PLEASE ADD YOUR NAME AND STUDENT NUMBER HERE:

George Adcock
b3018183

************************************************************************/

#include <fstream>			//for file output
#include <iostream>			//for console output
#include <conio.h>			//for kbhit
#include "hr_time.h"		//for stopwatches
#include <stdio.h>			//for fputs
#include <set>
#include "bubble.h"
#include "Radix.h"
#include "Quick.h"
#include "George.h"
#include <xmmintrin.h> // floating point intrinsics
#include <emmintrin.h> // int intrinsics
#include <omp.h>

enum sortingAlgrothimType
{
	bubble = 0,
	radix,
	Quick,
	George
};

using namespace std;
using namespace BubbleSort;

#define MAX_ROWS 2000
#define MAX_COLS 1000
#define total MAX_ROWS * MAX_COLS

#define MAX_CHARS 6			// numbers are in the range 1- 32,767, so 6 digits is enough.
#define ASCII 0x30			// add this to a single digit 0-9 to make an ASCII character

#define SortedRows "SortedRows.txt"
#define SortedAll  "SortedAll.txt"		// for future use

int _gData [MAX_ROWS][MAX_COLS];		// 2000 rows of 1000 numbers to sort!

const int rseed = 123;				// arbitrary seed for random number generator - PLEASE DON'T ALTER
									// After sorting data generated with seed 123 these results should be true:
const int	checkBeg = 87,			// at [0][0]
			checkMid = 16440,		// at [MAX_ROWS/2][MAX_COLS/2]
			checkEnd = 32760;		// at [MAX_ROWS-1][MAX_COLS-1]

std::set<double> q1Times;
std::set<double> q2Times;
std::set<double> q3Times;
std::set<double> q4Times;

#pragma region function declarations

void getData(void);
void displayCheckData(void); // only accurate when the using the correct scale (2000*1000)
void outputDataAsString(const char* filename);
void outputTimes(int numLoops, int sortRowUsed, int sortAllUsed);
std::string getAlgorithmUsed(const int typeUsed);

#pragma endregion

template <class U>
U askQuestionWithTAns(string question)
{
	U ans;
	cout << question.c_str() << endl;
	cin >> ans;
	cout << endl;
	return ans; 
}

int main(void)
{
	const int numLoops = askQuestionWithTAns<int>("how many loops should the test perform");
	int sortRowUsed = 0, sortAllUsed = 0;

	getData(); // populate the container with the data for this iteration
	outputDataAsString(SortedRows);

	for (int i = numLoops; i > 0; --i)
	{
		CStopWatch q1, q2, q3, q4; // new stopwatches to be made for each test to reduce the risk of times being corrupted

		cout << "getting Data" << endl;

		getData(); // populate the container with the data for this iteration
		
		cout << endl << "sorting rows" << endl;

		int* ptr = _gData[0];
		q1.startTimer();
		//BubbleSort::sortEachRow(MAX_ROWS, MAX_COLS, &ptr, sortRowUsed);
		//RadixSort::sortEachRow(MAX_ROWS, MAX_COLS, &ptr, sortRowUsed);
		QuickSort::sortEachRow(MAX_ROWS, MAX_COLS, &ptr, sortRowUsed);
		q1.stopTimer();

		displayCheckData();

		cout << endl << "outputting rows" << endl;

		q2.startTimer();
		outputDataAsString(SortedRows);
		q2.stopTimer();

		cout << endl << "sorting all" << endl;

		q3.startTimer();
		//BubbleSort::sortAll(total, &ptr, sortAllUsed);
		//RadixSort::sortAll(total, &ptr, sortAllUsed);
		GeorgeSort::sortAll(_gData[0], total, sortAllUsed);
		//QuickSort::sortAll(total, &ptr, sortAllUsed);
		q3.stopTimer();

		cout << endl << "outputting all" << endl;

		q4.startTimer();
		outputDataAsString(SortedAll);
		q4.stopTimer();

		q1Times.insert(q1.getElapsedTime());
		q2Times.insert(q2.getElapsedTime());
		q3Times.insert(q3.getElapsedTime());
		q4Times.insert(q4.getElapsedTime());
	}

	outputTimes(numLoops, sortRowUsed, sortAllUsed);

	while (!_kbhit());  //to hold console
}

//*********************************************************************************
void getData()		// Generate the same sequence of 'random' numbers.
{
	srand(123); //random number seed PLEASE DON'T CHANGE!
	for (int i = 0; i<MAX_ROWS; i++)
		for (int j = 0; j<MAX_COLS; j++)
			_gData[i][j] = rand(); //RAND_MAX = 32767
}

//*********************************************************************************

//*********************************************************************************
void displayCheckData()
{
	cout << "\n\ndata[0][0]                   = " << _gData[0][0] << "\t" << (_gData[0][0] == checkBeg ? "OK" : "BAD");
	cout << "\ndata[MAX_ROWS/2][MAX_COLS/2] = " << _gData[MAX_ROWS / 2][MAX_COLS / 2] << "\t" << (_gData[MAX_ROWS / 2][MAX_COLS / 2] == checkMid ? "OK" : "BAD");
	cout << "\ndata[MAX_ROWS-1][MAX_COLS-1] = " << _gData[MAX_ROWS - 1][MAX_COLS - 1] << "\t" << (_gData[MAX_ROWS - 1][MAX_COLS - 1] == checkEnd ? "OK" : "BAD");
}

//*********************************************************************************
void outputTimes(int numLoops, int sortRowUsed, int sortAllUsed)
{
	void generateResultsFile(int Loops, int sortRow, int sortAll);

	cout << "\n\nNumber of Loops: " << numLoops;
	cout << "\n\nData set size: " << MAX_ROWS * MAX_COLS;

	cout << "\n\nSort row Type Used: " << getAlgorithmUsed(sortRowUsed).c_str();
	cout << "\n\nSort All Type Used: " << getAlgorithmUsed(sortAllUsed).c_str();

	cout << "\n\nFastest time for sorting all rows: " << *q1Times.begin();

	cout << "\n\nFastest time for outputting rows to file: " << *q2Times.begin();

	cout << "\n\nFastest time for sorting all data: " << *q3Times.begin();

	cout << "\n\nFastest time for outputting all sorted data to file: " << *q4Times.begin();

	cout << "\n\nTotal time: " << *q1Times.begin() + *q2Times.begin() + *q3Times.begin() + *q4Times.begin() ;

	generateResultsFile(numLoops, sortRowUsed, sortAllUsed);

	cout << "\n\n\nPress a key to terminate.";
}

void generateResultsFile(int numLoops, int sortRowUsed, int sortAllUsed)
{
	ofstream file("resutls.txt", ofstream::out);

	file << "num loops = " << numLoops << endl;
	file << "Sort Row Used = " << getAlgorithmUsed(sortRowUsed).c_str() << endl;
	file << "Sort All Used = " << getAlgorithmUsed(sortAllUsed).c_str() << endl;
	file << "Data set size: " << MAX_ROWS * MAX_COLS << endl;
	file << "fastest all rows" << endl;
	file << "fastest output rows" << endl;
	file << "fastest sort all" << endl;
	file << "fastest output all" << endl;
	file << "total time" << endl;

	file << endl << endl << endl;

	file << *q1Times.begin() << endl;
	file << *q2Times.begin() << endl;
	file << *q3Times.begin() << endl;
	file << *q4Times.begin() << endl;
	file << *q1Times.begin() + *q2Times.begin() + *q3Times.begin() + *q4Times.begin() << endl;

	file.close();
}

//*********************************************************************************
//Builds a sorted number list as a long string then outputs the whole thing in one big fputs!

// 2) outputSortedRows() - output the sorted rows as SortedRows.txt using SIMD 
//	 methods to parallelise the conversion to ASCII
void outputDataAsString(const char* filename)
{
	string SIMDIntToASCII(int* params);

	string odata = SIMDIntToASCII(_gData[0]);

	ofstream file(filename, ofstream::out);
	file << odata.c_str();
	file.close();

}

std::string getAlgorithmUsed(const int typeUsed)
{
	switch(typeUsed)
	{
	case sortingAlgrothimType::bubble:
		return "Bubble";

	case sortingAlgrothimType::radix:
		return "Radix";

	case sortingAlgrothimType::Quick:
		return "Quick";

	case sortingAlgrothimType::George:
		return "George";
	}
}

string SIMDIntToASCII(int* start) 
{
	__m128i minusLastDigit  = _mm_set1_epi32(0);
	__m128i reducedNums;
	__m128i valuesToConvert;

	__m128 reducedNumA;
	__m128 reducedNumB;

	const __m128 simdReducer = _mm_set_ps1(0.1f);
	//const __m128i simdScaleUp = _mm_set1_epi16(10);

	std::string toReturn = "";

	int lineBreakCounter = 0;
	for (int i = 0; i < total; i += 8)
	{
		// import the values from the passed in pointer using the same pointer offset system used in the sort algorithms
		int params[8] = { *(start + i), *(start + i + 1), *(start + i + 2), *(start + i + 3), *(start + i + 4), *(start + i + 5), *(start + i + 6), *(start + i + 7) };
		// this step makes a copy of the passed in values - so not as to break the original data
		__m128i values = _mm_setr_epi16(params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7]);

		std::vector<string> stringComponents(8);

		// reserve the correct memory for the strings in this container upfront
		#pragma omp parallel sections
		{
			#pragma omp section
				stringComponents[0].reserve(6);
			#pragma omp section
				stringComponents[1].reserve(6);
			#pragma omp section
				stringComponents[2].reserve(6);
			#pragma omp section
				stringComponents[3].reserve(6);
			#pragma omp section
				stringComponents[4].reserve(6);
			#pragma omp section
				stringComponents[5].reserve(6);
			#pragma omp section
				stringComponents[6].reserve(6);
			#pragma omp section
				stringComponents[7].reserve(6);
		}

		// so that we can process every digit in each number
		for (int i = 1; i < MAX_CHARS; ++i)
		{
			#pragma region nonSIMD Method for reference

				//int baseNumber = 440;
				//const float reducer = 0.1;	// number used to scale down the current base number
				//const int scaleUp = 10;	// number used to scale up the reduced number

				// loop start (for every digit)

				// apply the reducer
				//int reducedNum1 = baseNumber * reducer; (440 * 0.1 = 44)

				// scale up the number without it's last digit
				//int minusLastDigit = reducedNum1 * scaleUp; (44 * 10 = 440)

				// extract the missing number
				//int value = baseNumber - minusLastDigit; (440 - 44 = 0)

				// reset the base number
				//baseNumber = reducedNum; baseNumber = 44

				//	string s = "";

				//s += value + ASCII;

				// loop end

			#pragma endregion

			// setup the values to be processed in this iteration - (implicit conversion to float here)
			__m128 valuesA = _mm_set_ps(values.m128i_i16[0], values.m128i_i16[1], values.m128i_i16[2], values.m128i_i16[3]);
			__m128 valuesB = _mm_set_ps(values.m128i_i16[4], values.m128i_i16[5], values.m128i_i16[6], values.m128i_i16[7]);

			// apply the reducer to the float values - allowing us to later extract the right most digit
			reducedNumA = _mm_mul_ps(valuesA, simdReducer);
			reducedNumB = _mm_mul_ps(valuesB, simdReducer);

			// store the reduced numbers in a new containter - implicit conversion to short here so we can "loose" the right-most digit
			reducedNums = _mm_setr_epi16
			(
				reducedNumA.m128_f32[3], reducedNumA.m128_f32[2], reducedNumA.m128_f32[1], reducedNumA.m128_f32[0],
				reducedNumB.m128_f32[3], reducedNumB.m128_f32[2], reducedNumB.m128_f32[1], reducedNumB.m128_f32[0]
			);

			// i can't get |minusLastDigit = _mm_mul_epu32(testIntValues, simdScaleUp);| to work correctly so added this to do the same job
			for (int i = 10; i > 0; --i)
				minusLastDigit = _mm_add_epi16(minusLastDigit, reducedNums);

			// extract the right most digit and then reset the container
			valuesToConvert = _mm_subs_epi16(values, minusLastDigit);
			minusLastDigit = _mm_set1_epi32(0.0f);

			values = reducedNums;

			// convert the extracted values to into an ASCII string and append them to the main string for each number
			#pragma omp parallel sections
			{
				#pragma omp section
					stringComponents[0] += valuesToConvert.m128i_i16[0] + ASCII;
				#pragma omp section	     
					stringComponents[1] += valuesToConvert.m128i_i16[1] + ASCII;
				#pragma omp section	     
					stringComponents[2] += valuesToConvert.m128i_i16[2] + ASCII;
				#pragma omp section	     
					stringComponents[3] += valuesToConvert.m128i_i16[3] + ASCII;
				#pragma omp section	     
					stringComponents[4] += valuesToConvert.m128i_i16[4] + ASCII;
				#pragma omp section	     
					stringComponents[5] += valuesToConvert.m128i_i16[5] + ASCII;
				#pragma omp section	     
					stringComponents[6] += valuesToConvert.m128i_i16[6] + ASCII;
				#pragma omp section	     
					stringComponents[7] += valuesToConvert.m128i_i16[7] + ASCII;
			}
		}

		// reorder the now formed string by swapping the first and last element as well as the next two in (the middle digit is always right)
		for (auto& param : stringComponents)
		{
			#pragma omp parallel sections
			{
				#pragma omp section
					std::swap(param.at(0), param.at(param.length() - 1));
				#pragma omp section
					std::swap(param.at(1), param.at(param.length() - 2));
			}
			param += "\t";
		}
		
		// append all the strings
		toReturn += stringComponents[0];
		toReturn +=	stringComponents[1];
		toReturn += stringComponents[2];
		toReturn += stringComponents[3];
		toReturn += stringComponents[4];
		toReturn += stringComponents[5];
		toReturn += stringComponents[6];
		toReturn += stringComponents[7];

		lineBreakCounter += 8;
		if(lineBreakCounter == 1000)
		{
			toReturn += "\n";
			lineBreakCounter = 0;
		}
	}

	return toReturn;
}
