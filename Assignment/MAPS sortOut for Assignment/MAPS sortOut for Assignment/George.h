#pragma once
#include <vector>
#include <algorithm>

// custom sort technique combining ideas of radix and bucket sort in a parallelisable way
namespace GeorgeSort
{
	const int algorithmType = 3;
	void sortAll(int arr[], int n, int& algorithmUsed)
	{
		algorithmUsed = 3;

		int blank;

		// 1) create the buckets to store the types of numbers (can be completed in parallel if not for scope issues)
		std::vector<int> units;
		std::vector<int> tens;
		std::vector<int> hundreds;
		std::vector<int> thousands;
		std::vector<int> tenThousands;
	
	// allocate the appropriate memory to each container in parallel
		#pragma omp parallel sections
		{
			#pragma omp section
				units.reserve(592);
			#pragma omp section
				tens.reserve(2000000);
			#pragma omp section
				hundreds.reserve(54741);
			#pragma omp section
				thousands.reserve(1939204);
			#pragma omp section
				tenThousands.reserve(1391263);
		}
		
		// 2) Put array elements in different buckets (can't be completed in parallel) - populate containers
		for (int i = 0; i < n; i++)
		{
			if (arr[i] < 10)
				units.push_back(arr[i]);
			else if (arr[i] > 9 && arr[i] < 100)
				tens.push_back(arr[i]);
			else if (arr[i] > 99 && arr[i] < 1000)
				hundreds.push_back(arr[i]);
			else if (arr[i] > 999 && arr[i] < 10000)
				thousands.push_back(arr[i]);
			else
				tenThousands.push_back(arr[i]);
		}

		// 3) Sort individual buckets in parallel, the documentation says that sections work best in chunks of 3 so an empty assignment was added
		#pragma omp parallel sections
		{
			#pragma omp section
				std::sort(units.begin(), units.end());
			#pragma omp section
				std::sort(tens.begin(), tens.end());
			#pragma omp section 
				blank = 0;
		}

		#pragma omp parallel sections
		{
			#pragma omp section
				std::sort(hundreds.begin(), hundreds.end());
			#pragma omp section
				std::sort(thousands.begin(), thousands.end());
			#pragma omp section
				std::sort(tenThousands.begin(), tenThousands.end());
		}
		
		// 4) Concatenate all buckets into arr[] (this can be partially parallelised)
		tens.insert(tens.begin(), units.begin(), units.end());
		#pragma omp parallel sections
		{
			#pragma omp section
				tens.insert(tens.end(), hundreds.begin(), hundreds.end());

			#pragma omp section
				thousands.insert(thousands.end(), tenThousands.begin(), tenThousands.end());
		}

		tens.insert(tens.end(), thousands.begin(), thousands.end());
			
		// 5) Write the new data over the old data
		int counter = 0;
		for(const auto& param : tens)
		{
			arr[counter] = param;
			++counter;
		}
	}
}
