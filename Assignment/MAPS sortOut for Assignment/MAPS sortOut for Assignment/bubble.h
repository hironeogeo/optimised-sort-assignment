#pragma once

#include "omp.h"
#include<iostream>
namespace BubbleSort
{
	const int algorithmType = 0;

	void sortEachRow(const int& rows, const int columns, int* _gData[], int& algorithmTypeUsed)
	{
		algorithmTypeUsed = algorithmType; // namespace specific key
		bool swapped;
		std::cout << std::endl << "Sorting data rows...";
		for (int i = 0; i < rows; i++) // for all the rows
		{
			int* row = _gData[0];
			swapped = false;
			//Use a bubble sort on a row 

#pragma omp for schedule(static, 125)
			for (int n = columns - 1; n >= 0; n--) // used as the count of how many times we need to check the row we're on
			{
				for (int j = 0; j < n; j++) // for each element in the row
				{
					int* ptr1 = row + ((i * columns) + j);
					int* ptr2 = row + ((i * columns) + j + 1);

					if (*ptr1 > *ptr2)
					{
						std::swap(*ptr1, *ptr2);
						swapped = true;
					}
				}

				// optimisation to bubble sort to break if no more swaps are detected
				if (swapped == false)
					break;
			}
		}
	}

	void sortAll(const int total, int* _gData[], int& algorithmTypeUsed)
	{
		algorithmTypeUsed = algorithmType; // namespace specific key
		std::cout << std::endl << "Sorting all data...";
		bool swapped;
		// const pointer to the start of our array. the value in the array can change but this pointer will always be pointing at it
		int* const ptrBase = _gData[0];
		for (int i = 0; i < (total - 1); ++i)
		{
			bool swapped = false;
			for (int j = 0; j < (total - i - 1); ++j)
			{
				int* ptr = ptrBase + j;
				int* ptr2 = ptrBase + j + 1;

				if (*ptr > *ptr2)
				{
					std::swap(*ptr, *ptr2);
					swapped = true;
				}
			}

			// optimisation to prevent unnecessary iterations when there won't be any more swaps
			if (!swapped)
				break;
		}
	}
}
