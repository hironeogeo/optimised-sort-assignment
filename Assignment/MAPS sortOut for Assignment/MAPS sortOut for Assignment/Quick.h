#pragma once
#include "omp.h"
#include<iostream>

namespace QuickSort
{
	const int algorithmType = 2;

	// A utility function to swap two elements
	void swap(int* a, int* b)
	{
		int t = *a;
		*a = *b;
		*b = t;
	}

	/* This function takes last element as pivot, places
	the pivot element at its correct position in sorted
	array, and places all smaller (smaller than pivot)
	to left of pivot and all greater elements to right
	of pivot */
	int partition(int arr[], int low, int high)
	{
		int pivot = arr[high];    // pivot
		int i = (low - 1);  // Index of smaller element

		for (int j = low; j <= high - 1; ++j)
		{
			// If current element is smaller than or
			// equal to pivot
			if (arr[j] <= pivot)
			{
				++i;    // increment index of smaller element
				swap(&arr[i], &arr[j]);
			}
		}
		swap(&arr[i + 1], &arr[high]);
		return (i + 1);
	}

	/* A[] --> Array to be sorted,
	l  --> Starting index,
	h  --> Ending index */
	void quickSortIterative(int arr[], int l, int h)
	{
		// Create an auxiliary stack
		//int stack[h - l + 1];
		int stack[1001];

		int top = -1; // initialize top of stack

		// push initial values of l and h to stack
		stack[++top] = l;
		stack[++top] = h;

		// Keep popping from stack while is not empty
		while (top >= 0)
		{
			// Pop h and l
			h = stack[top--];
			l = stack[top--];

			// Set pivot element at its correct position
			// in sorted array
			int p = partition(arr, l, h);

			// If there are elements on left side of pivot,
			// then push left side to stack
			if (p - 1 > l)
			{
				stack[++top] = l;
				stack[++top] = p - 1;
			}

			// If there are elements on right side of pivot,
			// then push right side to stack
			if (p + 1 < h)
			{
				stack[++top] = p + 1;
				stack[++top] = h;
			}
		}
	}

	/* The main function that implements QuickSortRecursive
	arr[] --> Array to be sorted,
	low  --> Starting index,
	high  --> Ending index */
	void quickSortReccursive(int arr[], int low, int high, int threadsAvaliable)
	{
		if (low < high)
		{
			/* pi is partitioning index, arr[p] is now
			at right place */
			int pi = partition(arr, low, high);

			// Separately sort elements before
			// partition and after partition

			if (threadsAvaliable < 2) // if we don't have enough threads to further delegate work to swap to an iterative approach
			{
				quickSortIterative(arr, low, high);
			}
			else
			{
				#pragma omp parallel sections // if we do have enough threads deleagte work appropriately
				{
					#pragma omp section
						quickSortReccursive(arr, low, pi - 1, threadsAvaliable * 0.5);
					#pragma omp section
						quickSortReccursive(arr, pi + 1, high, threadsAvaliable - (threadsAvaliable * 0.5));
				}
			}
		}
	}

	void sortEachRow(const int& rows, const int columns, int* _gData[], int& algorithmTypeUsed)
	{
		int* row;
		#pragma omp parallel sections // if we do have enough threads deleagte work appropriately
		{
			#pragma omp section
				algorithmTypeUsed = algorithmType; // namespace specific key
			#pragma omp section
				row = _gData[0];
		}


		#pragma omp for schedule(static, 250) // <<< a number of options were tested but it was found that 250  (n/4) was the biggest gain 
			for (int i = 0; i < rows; ++i) // for every row we want to process
			{
				quickSortReccursive(row, (i * columns) + 0 , (i * columns) + columns - 1, 8);
			}
	}

	void sortAll(const int total, int* _gData[], int& algorithmTypeUsed)
	{
		algorithmTypeUsed = algorithmType; // namespace specific key
		quickSortReccursive(_gData[0], 0, total - 1, 8);
	}
}

