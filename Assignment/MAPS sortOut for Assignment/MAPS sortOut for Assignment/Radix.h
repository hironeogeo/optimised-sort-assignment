#pragma once
#include <vector>
#include<iostream>

namespace RadixSort
{
	// A utility function to get maximum value in arr[]
	int getMax(int arr[], int n)
	{
		int mx = arr[0];
		for (int i = 1; i < n; i++)
			if (arr[i] > mx)
				mx = arr[i];
		return mx;
	}

	// A function to do counting sort of arr[] according to
	// the digit represented by exp.
	void countSortRow(int arr[], int n, int exp)
	{
		int output[1000];

		//int output[n]; // output array
		int i, count[32767] = { 0 };

		// Store count of occurrences in count[]
		for (i = 0; i < n; ++i)
			count[(arr[i] / exp) % 10]++;

		// Change count[i] so that count[i] now contains actual
		//  position of this digit in output[]
		for (i = 1; i < 10; ++i)
			count[i] += count[i - 1];

		// Build the output array
		for (i = n - 1; i >= 0; --i)
		{
			output[count[(arr[i] / exp) % 10] - 1] = arr[i];
			count[(arr[i] / exp) % 10]--;
		}

		// Copy the output array to arr[], so that arr[] now
		// contains sorted numbers according to current digit
		for (i = 0; i < n; ++i)
			arr[i] = output[i];
	}

	// A function to do counting sort of arr[] according to
	// the digit represented by exp.
	void countSortAll(int arr[], int n, int exp)
	{
		std::vector<int> output(2000000);
		int i, count[32767] = { 0 };

		// Store count of occurrences in count[]
		for (i = 0; i < n; ++i)
			count[(arr[i] / exp) % 10]++;

		// Change count[i] so that count[i] now contains actual
		//  position of this digit in output[]
		for (i = 1; i < 10; ++i)
			count[i] += count[i - 1];

		// Build the output array
		for (i = n - 1; i >= 0; --i)
		{
			output[count[(arr[i] / exp) % 10] - 1] = arr[i];
			count[(arr[i] / exp) % 10]--;
		}

		// Copy the output array to arr[], so that arr[] now
		// contains sorted numbers according to current digit
		for (i = 0; i < n; ++i)
			arr[i] = output[i];
	}

	const int algorithmType = 1;

	void sortEachRow(const int& rows, const int columns, int* _gData[], int& algorithmTypeUsed)
	{
		algorithmTypeUsed = algorithmType; // namespace specific key
		int m; // variable to store the max value on that row

		// loop structure to manage the process of each row
#pragma omp for
		for (int i = 0; i < rows; ++i)
		{
			// Find the maximum number to know number of digits
			m = getMax(_gData[0], columns);

			// Do counting sort for every digit. Note that instead
			// of passing digit number, exp is passed. exp is 10^i
			// where i is current digit number
			for (int exp = 1; m / exp > 0; exp *= 10)
				countSortRow(_gData[0] + (i * columns), columns, exp);
		}
	}

	void sortAll(const int total, int* _gData[], int& algorithmTypeUsed)
	{
		algorithmTypeUsed = algorithmType; // namespace specific key

		// Find the maximum number to know number of digits
		int m = getMax(_gData[0], total);

		// Do counting sort for every digit. Note that instead
		// of passing digit number, exp is passed. exp is 10^i
		// where i is current digit number
		for (int exp = 1; m / exp > 0; exp *= 10)
			countSortAll(_gData[0], total, exp);

	}
}

