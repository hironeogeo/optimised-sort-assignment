# Optimised-Search-Assignment

This was a university assignment that I really enjoyed. We had a set of numbers that had to be sorted in the fastest time possible

We were given the collection of numbers and then the timing code was fixed but we had to supply the implemenation.

There was also some verification checks to ensure that the sorting was successful.

My approach to this was to:
- Research existing sorting algorithms
- Implement them inside the solution
- Test each one to see how they compared to each other

Quite quickly I realised that in order to get reliable results i'd have to run the test for each algorithm multiple times and then take the average result. So I built a test harness that would ask the user how many runs to do and then the testing would start

I also implemented each algorithm inside it's own namespace so they could quickly be hot swapped out but also so it was clear which one was being used.

Once i'd completing this first round of testing between bubble, quick and radix sort, radix was the fastest. I had also discovered something called bucket sort which involved splitting the data set into "buckets" and then running a sorting algorithm on each bucket. 

I created a new namespace called George sort to implement this new bucket + sort algortihm approach so that it was separate from the others and after some testing actually found that on these smaller data sets the std::sort algorithm was even faster than the radix sort.

My last optimisation was to implement OpenMP multi-threading to enable the buckets to be sorted in parrallel rather than seqwuence which enabled this last sorting approach to be even faster than the last.

## Limitations

These optimisations were designed for this specific data set and the hardware at the time (~2018) with a different data set and hardware, other techniques may have been more appropriate but the lessons and methodology could be repeated again to hopefully find an equally optimal solution